package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {

        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        throw new UnsupportedOperationException();
    }

    public Optional<ProjectDto> findTheBiggest() {
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        throw new UnsupportedOperationException();
    }

    public List<ProjectSummaryDto> getSummary() {
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
        throw new UnsupportedOperationException();
    }

    public int getCountWithRole(String role) {
        // TODO: Use a single query
        throw new UnsupportedOperationException();
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto request) {
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
        Technology technology = Technology.builder()
                .description(request.getTechDescription())
                .name(request.getTech())
                .link(request.getTechLink())
                .build();
        technology = technologyRepository.save(technology);

        Project project = Project.builder()
                .description(request.getProjectDescription())
                .name(request.getProjectName())
                .build();
        project = projectRepository.save(project);

        Team team = Team.builder()
                .area(request.getTeamArea())
                .technology(technology)
                .room(request.getTeamRoom())
                .name(request.getTeamName())
                .project(project)
                .build();

        teamRepository.saveAndFlush(team);
        return project.getId();
    }
}

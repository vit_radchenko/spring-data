package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    //TODO: тоже в шоке что оно так работает))
    @Modifying
    @Query("update Office o set o.address = ?2 where o IN (SELECT o1 From Office o1 INNER JOIN o1.users WHERE o1.address = ?1)")
    void updateOffice(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);

    //TODO: OMG, Я в ШОКЕ ЧТО ОНО РАБОТАЕТ, честно (просто магия)
    @Query("Select DISTINCT o From Office o INNER JOIN o.users u INNER JOIN u.team t WHERE t.technology.name = :technology")
    List<Office> findAllByTechnology(String technology);
}

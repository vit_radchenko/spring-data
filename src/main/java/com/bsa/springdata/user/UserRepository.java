package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findAllByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("Select u from User u where u.office.city = :city order by u.lastName asc")
    List<User> findAllByCity(String city);

    List<User> findAllByExperienceGreaterThanEqual(int experience, Sort pageable);

    @Query("Select u from User u where u.office.city = :city and u.team.room = :room order by u.lastName asc")
    List<User> findAllByCityAndRoom(String city, String room, Sort lastName);

    @Modifying
    @Query("delete from User u where u.experience < ?1")
    int deleteByExperienceLessThan(int experience);
}

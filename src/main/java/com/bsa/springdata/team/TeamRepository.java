package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String name);

    int countByTechnologyName(String technology);

    List<Team> findByUsersLessThan(int devsNumber);

//    @Query("Select p From Project p where p IN (Select t From Team t where t.technology.name = :technology ORDER BY SIZE(t.users))")
//    @Query("Select p From Project p where p.teams.technology.name = :technology")
//    List<Project> topByTechnology(String technology, Pageable pageable);
}

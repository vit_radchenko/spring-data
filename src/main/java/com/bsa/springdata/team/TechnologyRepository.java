package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {
    Optional<Technology> findByName(String name);

//    @Modifying
////    @Query("update Technology t set t.name = ?3 FROM Team t2 where t2.technology = t AND size(t2.users) < ?1 AND t.name = ?2")
//    void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName);
}
